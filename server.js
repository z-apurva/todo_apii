const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));

mongoose.connect("mongodb://localhost:27017/todo_apii",{ useNewUrlParser:true , useUnifiedTopology:true},(err,data)=>{
    if(err) {
        console.log("not connected")
    }
    else {
        console.log("connected")
    }
})
mongoose.set('useCreateIndex', true);
var routes = require('./routes/routes')
app.use('/',routes)

app.listen(9230,()=>{
    console.log("server started")
})


// 'use strict';
// const express     = require('express');
// const app         = express();
// const cors        = require('cors');
// const path        = require('path');
// const http        = require('http');

// const bodyParser = require('body-parser');
// app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({
//     extended: true
// }));

// app.use('/', require('./routes/routes'));

// var server = http.createServer(app);
// var port = process.env.PORT || 3860;

// server.listen(port, function () {
//   console.log('now listening on port:' + port);
// });

// module.exports = app;