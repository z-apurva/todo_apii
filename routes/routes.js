var express = require("express")
var app = express.Router()


var addtask = require('./Task/addtasks')
app.post('/addtask',addtask)

var gettask = require('./Task/gettasks')
app.get('/gettask',gettask.gettask)

var updatetask = require('./Task/updatetasks')
app.post('/updatetask',updatetask)

var deletetask = require('./Task/deletetasks')
app.delete('/deletetask',deletetask.deletetask)

module.exports = app;